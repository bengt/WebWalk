import urlparse

from lxml import html, etree
import httplib2
from httplib2 import socket

class WebWalk:
    def __init__(self, cache=None, timeout=None, proxy_info=None, ca_certs=None, disable_ssl_certificate_validation=False, max_depth=None, restrict_domain=True):
        self.httplib2 = httplib2.Http(cache=cache, timeout=timeout, proxy_info=proxy_info, ca_certs=ca_certs, disable_ssl_certificate_validation=disable_ssl_certificate_validation)
        self.visited_urls = []
        
    def walk(self, link):
        print("DEBUG: walk getting called with url " + link)
        
        try:
            self.base_url
        except AttributeError:
            (scheme, netloc, path, params, query, fragment) = urlparse.urlparse(link)
            self.base_url = scheme + "://" + netloc
            print("DEBUG: self.baseurl: " + self.base_url)

        
        # handle relative urls
        try:
            if link.startswith("/") or (not link.startswith("http://") and not
                                        link.startswith("https://")):
                link = self.base_url + link
        except AttributeError as e:
            print(e)

        if link in self.visited_urls:
            try:
                self.skip_count += 1
            except AttributeError:
                self.skip_count = 1
            print("DEBUG: self.skip_count: " + str(self.skip_count))
            return
        
        print("DEBUG: walk opening url " + link)
        try:
            resp, content = self.httplib2.request(link, "GET")
            del resp
        except socket.error as error:
            print(error)
            return
        
        self.visited_urls.append(link)
        print("DEBUG: self.visited_urls:", len(self.visited_urls))
        
        try:
            html_dom = html.document_fromstring(content)
        except etree.ParserError as e:
            print(e)
            return
        for (element, attribute, link, pos) in html.iterlinks(html_dom):
            for url in self.walk(link):
                yield url
            yield link


if __name__ == "__main__":
    WW = WebWalk(cache=".HttpLib2.cache")
    for url in WW.walk("http://www.dmoz.de/"):
        print("DEBUG: main receiving URL " + url)
